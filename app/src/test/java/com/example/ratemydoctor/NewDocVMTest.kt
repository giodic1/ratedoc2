package com.example.ratemydoctor

import androidx.compose.ui.ExperimentalComposeUiApi
import com.example.ratemydoctor.docview.NewReview
import com.example.ratemydoctor.newdoc.NewDoc
import com.example.ratemydoctor.ui.theme.Doc
import org.junit.Assert
import org.junit.Test

class NewDocVMTest {

    @ExperimentalComposeUiApi
    @Test
    fun TestDocNameNeeded() {
        //arrange
        val vm = NewDoc()
        vm.setName("")

        //act
        Assert.assertThrows(Exception::class.java) {
            val doc = vm.validate()
        }

        //assert
        try {
            val doc = vm.validate()
        } catch (e: Exception) {
            Assert.assertEquals("Doc name needed", e.message)
        }
    }

    @ExperimentalComposeUiApi
    @Test
    fun TestReviewRatingNeeded() {
        //arrange
        val vm = NewReview()
        vm.setRating("")
        vm.setComment("")
        val doc = Doc("")

        //act
        Assert.assertThrows(Exception::class.java) {
            val rating = vm.validate(doc)
        }

        //assert
        try {
            val rating = vm.validate(doc)
        } catch (e: Exception) {
            Assert.assertEquals("Rating needed", e.message)
        }
    }
}