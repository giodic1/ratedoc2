package com.example.ratemydoctor

import com.example.ratemydoctor.repository.DocsMemoryRepo
import com.example.ratemydoctor.ui.theme.Doc
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.junit.Assert.*
import java.util.*

class DocRepoTest {

    @ExperimentalCoroutinesApi
    @Test
    fun DocRepoInitialized() = runBlockingTest {
        //arrange
        val repo = DocsMemoryRepo()

        // act
        val docs = repo.getDocs()

        //assert
        assertEquals(docs.size, 1)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun AddDocToList() = runBlockingTest {
        //arrange
        val repo = DocsMemoryRepo()
        val doc = Doc("Test")

        //act
        repo.addDoc(doc)

        //assert
        val docs = repo.getDocs()
        assertEquals(docs.size, 2)
        assertEquals(docs[0].name, "Test")
    }

    @ExperimentalCoroutinesApi
    @Test
    fun DeleteDocFromList() = runBlockingTest {
        //arrange
        val repo = DocsMemoryRepo()
        val doc = Doc("Bert")

        //act
        repo.deleteDoc(doc)

        //assert
        val docs = repo.getDocs()
        assertEquals(docs.size, 0)
    }
}