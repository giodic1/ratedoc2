package com.example.ratemydoctor

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.ratemydoctor.doclist.DocListVM
import com.example.ratemydoctor.docview.DocVM
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating
import dagger.hilt.android.testing.HiltAndroidRule
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class DocVMTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun TestSelectDoc() {
        composeTestRule.mainClock.autoAdvance = false

        composeTestRule.setContent {
            //arrange
            val vm: DocVM = viewModel()
            Assert.assertNull(vm.selected.value)
            val rating = Rating("Test", 10.0, "")

            //act
            vm.selectReview(rating)

            //assert
            Assert.assertEquals("Test",vm.selected.value?.docName)
        }
    }
}