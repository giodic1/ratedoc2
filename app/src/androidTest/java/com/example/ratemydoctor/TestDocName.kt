package com.example.ratemydoctor

import androidx.compose.ui.ExperimentalComposeUiApi
import com.example.ratemydoctor.docview.NewReview
import com.example.ratemydoctor.navigation.Routes
import com.example.ratemydoctor.newdoc.NewDoc
import com.example.ratemydoctor.newdoc.NewDocView
import com.example.ratemydoctor.ui.theme.Doc
import org.junit.Assert
import org.junit.Test

class TestDocName {

    @ExperimentalComposeUiApi
    @Test
    fun TestDoc() {
        //arrange
        val vm = NewDoc()
        vm.setName("Test")

        //act
        val doc = vm.validate()

        //assert
        Assert.assertEquals("Test", doc.name)
    }

    @ExperimentalComposeUiApi
    @Test
    fun TestReview() {
        //arrange
        val vm = NewReview()
        vm.setComment("")
        vm.setRating("10.0")

        //act
        val doc = Doc("Name")
        val rating = vm.validate(doc)

        //assert
        Assert.assertEquals(10.0, rating.rating)
    }

    @ExperimentalComposeUiApi
    @Test
    fun TestComment() {
        //arrange
        val vm = NewReview()
        vm.setComment("")
        vm.setRating("10.0")

        //act
        val doc = Doc("Name")
        val rating = vm.validate(doc)

        //assert
        Assert.assertEquals("Test Comment", rating.comments)
    }
}