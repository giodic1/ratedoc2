package com.example.ratemydoctor

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.ratemydoctor.newdoc.NewDoc
import com.example.ratemydoctor.newdoc.NewDocView
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class AddDocTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @ExperimentalComposeUiApi
    @Test
    @Composable
    fun TestDocValidation() {
        //arrange
        //act
        composeTestRule.setContent {
            NewDocView(
                onAddDoc = { d ->
                    assertTrue(false)
                },
                onAddReview = {}
            )
        }

        //assert
        composeTestRule.onNodeWithContentDescription("Name Input").performTextInput("Test")
        composeTestRule.onNodeWithText("Add Doc").performClick()
    }
}