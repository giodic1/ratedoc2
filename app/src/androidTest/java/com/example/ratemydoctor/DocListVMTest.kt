package com.example.ratemydoctor

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.ratemydoctor.doclist.DocListVM
import com.example.ratemydoctor.ui.theme.Doc
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class DocListVMTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun TestSelectDoc() {
        composeTestRule.mainClock.autoAdvance = false

        composeTestRule.setContent {
            //arrange
            val vm: DocListVM = viewModel()
            Assert.assertNull(vm.selected.value)
            val doc = Doc("Test")

            //act
            vm.selectDoc(doc)

            //assert
            Assert.assertEquals("Test",vm.selected.value?.name)
        }
    }
}