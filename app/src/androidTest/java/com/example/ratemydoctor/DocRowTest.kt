package com.example.ratemydoctor

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.ratemydoctor.ui.theme.Doc
import dagger.hilt.android.testing.HiltAndroidRule
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DocRowTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @ExperimentalFoundationApi
    @Test
    fun TestOnDeleteDoc() {
        //arrange
        val doc = Doc("Test")

        //act
        composeTestRule.setContent {
            DocRow(
                1,
                doc,
                onDelete = {
                    //assert
                    assertEquals(it, 1)
                },
                onSelectDoc = {}
            )
        }
    }

    @ExperimentalFoundationApi
    @Test
    fun TestOnSelectDoc() {
        //arrange
        val doc = Doc("Test")

        //act
        composeTestRule.setContent {
            DocRow(
                1,
                doc,
                onDelete = {},
                onSelectDoc = { d ->
                    //assert
                    assertEquals(d.name, doc.name)
                }
            )
        }
    }
}