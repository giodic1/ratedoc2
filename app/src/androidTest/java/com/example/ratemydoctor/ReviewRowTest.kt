package com.example.ratemydoctor

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.ratemydoctor.docview.ReviewRow
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating
import dagger.hilt.android.testing.HiltAndroidRule
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReviewRowTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @ExperimentalFoundationApi
    @Test
    fun TestOnDeleteReview() {
        //arrange
        val rating = Rating("TestName", 10.0, "")
        val doc = Doc("TestName")

        //act
        composeTestRule.setContent {
            ReviewRow(
                doc,
                1,
                rating,
                onDelete = {
                    //assert
                    assertEquals(it, 1)
                },
                onSelectReview = {}
            )
        }
    }

    @ExperimentalFoundationApi
    @Test
    fun TestOnSelectReview() {
        //arrange
        val doc = Doc("TestName")
        val rating = Rating(doc.name, 10.0, "")

        //act
        composeTestRule.setContent {
            ReviewRow(
                doc,
                1,
                rating,
                onDelete = {},
                onSelectReview = {
                    //assert
                    assertEquals(it.docName, rating.docName)
                }
            )
        }
    }
}