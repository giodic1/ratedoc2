package com.example.ratemydoctor.confirmdialog

import androidx.compose.runtime.State

interface IConfirmViewModel {
    val showConfirmDialog: State<Boolean>
    val waiting: State<Boolean>
    val waitingProgress: State<Float>
    fun showConfirmDelete(onConfirm: suspend () -> Unit)
    fun onConfirmDelete()
    fun onCancelDelete()
    fun dismissDialog()
}
