package com.example.ratemydoctor

import androidx.room.*
import androidx.room.Dao
import androidx.room.Database
import androidx.lifecycle.LiveData
import com.example.ratemydoctor.ui.theme.Doc

@Dao
interface DocsDao {
    @Query("SELECT name FROM doc")
    suspend fun getDocs(): List<Doc>

    @Insert
    suspend fun addDoc(doc: Doc)

    @Delete
    suspend fun deleteDoc(doc: Doc)

}

@Database(entities = [Doc::class], version = 3, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract fun docDao(): DocsDao
}