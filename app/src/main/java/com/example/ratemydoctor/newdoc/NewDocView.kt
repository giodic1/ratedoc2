package com.example.ratemydoctor.newdoc

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.ratemydoctor.docview.NewReview
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating
import java.lang.Exception

@ExperimentalComposeUiApi
@Composable
fun NewDocView(
    vm: NewDoc = viewModel(),
    RVM: NewReview = viewModel(),
    onAddDoc: (Doc) -> Unit,
    onAddReview: (Rating) -> Unit
) {
    val ctx = LocalContext.current
    val (nameTf, ratingTF, commentTf) = remember { FocusRequester.createRefs() }
    val keyboardController = LocalSoftwareKeyboardController.current
    LaunchedEffect(true) {
        nameTf.requestFocus()
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            "New Doctor",
            fontSize = 28.sp,
            modifier = Modifier
                .padding(16.dp)
                .focusRequester(nameTf)
        )
        OutlinedTextField(
            value = vm.name.value,
            onValueChange = vm::setName,
            placeholder = { Text("Name") },
            label = { Text("Name") },
            singleLine = true,
            modifier = Modifier.padding(14.dp),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Text
            ),
            keyboardActions = KeyboardActions( onNext = { ratingTF.requestFocus() } )
        )

        //ratings and comments stored separately in Ratings
        OutlinedTextField(
            value = RVM.rating.value,
            onValueChange = RVM::setRating,
            placeholder = { Text("Rating") },
            singleLine = true,
            label = { Text("10.0") },
            modifier = Modifier
                .padding(14.dp)
                .focusRequester(ratingTF),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Number
            ),
            keyboardActions = KeyboardActions(
                onNext = { commentTf.requestFocus() }
            )
        )
        OutlinedTextField(
            value = RVM.comment.value,
            onValueChange = RVM::setComment,
            placeholder = {
                Text("Comments")
            },
            label = {
                Text("Comments (Optional)")
            },
            singleLine = true,
            modifier = Modifier
                .padding(14.dp)
                .focusRequester(commentTf),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { keyboardController?.hide() }
            )
        )
        Button(
            onClick = {
                try {
                    val doc = vm.validate()
                    onAddDoc(doc)
                    val rating = RVM.validate(doc)
                    onAddReview(rating)
                } catch(e: Exception) {
                    Toast.makeText(ctx, e.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        ) {
            Text("Add Doctor")
        }
    }
}