package com.example.ratemydoctor.newdoc

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.ratemydoctor.ui.theme.Doc

class NewDoc: ViewModel() {
    private val _name: MutableState<String> = mutableStateOf("")
    val name: State<String> = _name

    fun setName(name: String) {
        _name.value = name
    }

    fun validate(): Doc {
        if (name.value.isEmpty()) {
            throw Exception("Name needed")
        }
        return Doc(name.value)
    }
}