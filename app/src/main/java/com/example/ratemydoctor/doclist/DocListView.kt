package com.example.ratemydoctor.doclist

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalConfiguration
import androidx.navigation.NavHostController
import com.example.ratemydoctor.DocRow
import com.example.ratemydoctor.confirmdialog.ConfirmDialog
import com.example.ratemydoctor.confirmdialog.ConfirmViewModel
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.LandscapeView
import com.example.ratemydoctor.ui.theme.SearchBar

@ExperimentalFoundationApi
@Composable
fun DocListView(
    docs: List<Doc>,
    selectedDoc: Doc?,
    confirmVM: ConfirmViewModel,
    waiting: Boolean,
    waitingProgress: Float,
    onDelete: suspend (Doc) -> Unit,
    onFilter: (String) -> Unit,
    onSelectDoc: (Doc) -> Unit,
    nav: NavHostController
) {
    confirmVM.waiting.value = waiting
    confirmVM.waitingProgress.value = waitingProgress
    Box(contentAlignment = Alignment.Center) {
        ConfirmDialog(
            title = "Deleting",
            text = "Are you sure you want to delete?",
            confirmViewModel = confirmVM
        )
        Column(
            modifier = Modifier.alpha(if(waiting) 0.2f else 1.0f)
        ) {
            SearchBar(onFilter = onFilter)
            val con = LocalConfiguration.current
            if (con.orientation == Configuration.ORIENTATION_PORTRAIT) {
                LazyColumn {
                    itemsIndexed(docs) { i, doc ->
                        DocRow(i, doc, {
                            confirmVM.showConfirmDelete(onConfirm = { onDelete(doc) })
                        }, onSelectDoc, nav)
                    }
                }
            } else {
                LandscapeView(selectedDoc?.name) {
                    LazyColumn {
                        itemsIndexed(docs) { i, doc ->
                            DocRow(i, doc, {
                                confirmVM.showConfirmDelete(onConfirm = { onDelete(doc) })
                            }, onSelectDoc, nav)
                        }
                    }
                }
            }
        }
        if (waiting) {
            CircularProgressIndicator()
        }
    }
}