package com.example.ratemydoctor.doclist

import android.app.Application
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.ratemydoctor.repository.DBRepo
import com.example.ratemydoctor.ui.theme.Doc
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DocListVM(app: Application): AndroidViewModel(app) {
    private val _docs: MutableState<List<Doc>> = mutableStateOf(listOf())
    val docs: State<List<Doc>> = _docs
    private val _selected: MutableState<Doc?>
    val selected: State<Doc?>
    private val _waiting: MutableState<Boolean>
    val waiting: State<Boolean>
    private val _waitingProgress: MutableState<Float>
    val waitingProgress: State<Float>

    private val _repository = DBRepo(app)

    init {
        viewModelScope.launch {
            _docs.value = _repository.getDocs()
        }
        _selected = mutableStateOf(null)
        selected = _selected
        _waiting = mutableStateOf(false)
        waiting = _waiting
        _waitingProgress = mutableStateOf(0.0f)
        waitingProgress = _waitingProgress
    }

    fun addDoc(doc: Doc) {
        viewModelScope.launch {
            _waiting.value = true
            _repository.addDoc(doc)
            _docs.value = _repository.getDocs()
            _waiting.value = false
        }
    }

    fun deleteDoc(doc: Doc) {
        viewModelScope.launch {
            _waiting.value = true
            _waitingProgress.value = 0.0f
            val progJob = viewModelScope.async { increment() }
            val deleteJob = viewModelScope.async { _repository.deleteDoc(doc) }
            progJob.start()
            deleteJob.await()
            progJob.cancel()
            _docs.value = _repository.getDocs()
            _waiting.value = false
            _waitingProgress.value = 0.0f
        }
    }

    private suspend fun increment() {
        while (true) {
            delay(500)
            _waitingProgress.value += (1.0f / 10.0f)
        }
    }

    fun filter(search: String) {
        viewModelScope.launch {
            _docs.value = _repository.getDocs().filter {
                    a -> a.name.contains(search, true)
            }
        }
    }

    fun selectDoc(doc: Doc) {
        _selected.value = doc
    }
}