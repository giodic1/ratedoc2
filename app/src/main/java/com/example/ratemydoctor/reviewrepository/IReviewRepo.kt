package com.example.ratemydoctor.reviewrepository

import com.example.ratemydoctor.ui.theme.Doc
import androidx.lifecycle.LiveData
import com.example.ratemydoctor.ui.theme.Rating

interface IReviewRepo {
    suspend fun getReviews(): List<Rating>
    suspend fun deleteReview(rating: Rating)
    suspend fun addReview(rating: Rating)
}