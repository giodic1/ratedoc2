package com.example.ratemydoctor.reviewrepository

import androidx.room.*
import androidx.room.Dao
import androidx.room.Database
import androidx.lifecycle.LiveData
import com.example.ratemydoctor.ui.theme.Rating

@Dao
interface ReviewsDao {
    @Query("SELECT docName, rating, comments FROM rating")
    suspend fun getReviews(): List<Rating>

    @Insert
    suspend fun addReview(rating: Rating)

    @Delete
    suspend fun deleteReview(rating: Rating)

}

@Database(entities = [Rating::class], version = 2, exportSchema = false)
abstract class ReviewDB : RoomDatabase() {
    abstract fun reviewDao(): ReviewsDao
}