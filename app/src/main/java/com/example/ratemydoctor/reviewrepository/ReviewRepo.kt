package com.example.ratemydoctor.reviewrepository

import com.example.ratemydoctor.ui.theme.Rating

class ReviewRepo(): IReviewRepo {
    private var _reviews = listOf<Rating>()

    @Override
    override suspend fun getReviews(): List<Rating> {
        return _reviews
    }

    @Override
    override suspend fun deleteReview(rating: Rating) {
        val i = _reviews.indexOf(rating)
        _reviews = _reviews.subList(0, i) + _reviews.subList(i + 1, _reviews.size)
    }

    @Override
    override suspend fun addReview(rating: Rating) {
        _reviews = listOf(rating) + _reviews
    }
}