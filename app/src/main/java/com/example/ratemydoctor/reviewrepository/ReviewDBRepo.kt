package com.example.ratemydoctor.reviewrepository

import android.app.Application
import androidx.room.Room
import com.example.ratemydoctor.Database
import com.example.ratemydoctor.ui.theme.Rating

class ReviewDBRepo (app: Application): IReviewRepo {
    private val db: ReviewDB
    private var _reviews: List<Rating> = listOf()

    init {
        db = Room.databaseBuilder(
            app,
            ReviewDB::class.java,
            "review.db"
        ).fallbackToDestructiveMigration().build()
    }

    @Override
    override suspend fun getReviews(): List<Rating> {
        return db.reviewDao().getReviews()
    }

    @Override
    override suspend fun deleteReview(rating: Rating) {
        db.reviewDao().deleteReview(rating)
    }

    @Override
    override suspend fun addReview(rating: Rating) {
        db.reviewDao().addReview(rating)
    }
}
