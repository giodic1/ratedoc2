package com.example.ratemydoctor.navigation

sealed class Routes(val route: String) {
    object DocList: Routes("doclist")
    object NewDoc: Routes("newdoc")
    object DocPage: Routes("docpage")
    object NewReview: Routes("newreview")
}