package com.example.ratemydoctor.navigation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.ratemydoctor.confirmdialog.ConfirmViewModel
import com.example.ratemydoctor.doclist.DocListVM
import com.example.ratemydoctor.doclist.DocListView
import com.example.ratemydoctor.docview.DocVM
import com.example.ratemydoctor.docview.DocView
import com.example.ratemydoctor.docview.NewReview
import com.example.ratemydoctor.docview.NewReviewView
import com.example.ratemydoctor.newdoc.NewDoc
import com.example.ratemydoctor.newdoc.NewDocView
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating

@ExperimentalComposeUiApi
@ExperimentalFoundationApi
@Composable
fun NavGraph(
    navController: NavHostController = rememberNavController()
) {
    val vm: DocListVM = viewModel()
    val RVM: DocVM = viewModel()

    //hardcode(vm, RVM)

    NavHost(
        navController = navController,
        startDestination = Routes.DocList.route
    ) {
        composable(Routes.DocList.route) {
            DocListScreen(vm, navController)
        }
        composable(Routes.NewDoc.route) {
            val newDocVM: NewDoc = viewModel()
            val newReviewVM: NewReview = viewModel()
            NewDocView(
                newDocVM,
                newReviewVM,
                onAddDoc = { doc ->
                    vm.addDoc(doc)
                    navController.navigate(Routes.DocList.route)
                },
                onAddReview = { rating ->
                    RVM.addReview(rating)
                }
            )
        }
        composable(Routes.DocPage.route) {
            DocScreen(RVM, vm, navController)
        }
        composable(Routes.NewReview.route) {
            val newReviewVM: NewReview = viewModel()
            NewReviewView(
                newReviewVM,
                onAddReview = { rating ->
                    RVM.addReview(rating)
                    navController.navigate(Routes.DocPage.route)
                },
                vm,
                navController
            )
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun DocScreen(
    RVM: DocVM,
    vm: DocListVM,
    nav: NavHostController
) {
    val confirmVM: ConfirmViewModel = viewModel()
    val ratings by RVM.ratings
    val selectedReview by RVM.selected
    DocView(
        ratings,
        selectedReview,
        confirmVM,
        waiting = RVM.waiting.value,
        waitingProgress = RVM.waitingProgress.value,
        onDelete = RVM::deleteReview,
        onFilter = RVM::filter,
        onSelectReview = RVM::selectReview,
        vm,
        nav
    )
}

@ExperimentalFoundationApi
@Composable
fun DocListScreen(
    vm: DocListVM,
    nav: NavHostController
) {
    val confirmVM: ConfirmViewModel = viewModel()
    val docs by vm.docs
    val selectedDoc by vm.selected
    DocListView(
        docs,
        selectedDoc,
        confirmVM,
        waiting = vm.waiting.value,
        waitingProgress = vm.waitingProgress.value,
        onDelete = vm::deleteDoc,
        onFilter = vm::filter,
        onSelectDoc = vm::selectDoc,
        nav
    )
}

private fun hardcode(
    vm: DocListVM,
    RVM: DocVM
) {
    val harold = Doc("Harold Shipman")
    val christopher = Doc("Christopher Duntsch")
    val michael = Doc("Michael Swango")
    val grant = Doc("Grant Iodice")
    val kenadi = Doc("Kenadi Wilkerson")
    vm.addDoc(harold)
    vm.addDoc(christopher)
    vm.addDoc(michael)
    vm.addDoc(grant)
    vm.addDoc(kenadi)

    val rHarold = Rating(harold.name, 0.0, "Stay away!")
    val rHarold2 = Rating(harold.name, 0.2, "Creepy!")
    val rHarold3 = Rating(harold.name, 2.0, "")
    val rChris = Rating(christopher.name, 0.0, "Terrible breath!")
    val rMichael = Rating(michael.name, 0.0, "")
    val rMichael2 = Rating(michael.name, 5.0, "")
    val rGrant = Rating(grant.name, 10.0, "Awesome!")
    val rGrant2 = Rating(grant.name, 9.3, "Really good!")
    val rKenadi = Rating(kenadi.name, 10.0, "Stellar!")
    RVM.addReview(rHarold)
    RVM.addReview(rHarold2)
    RVM.addReview(rHarold3)
    RVM.addReview(rChris)
    RVM.addReview(rMichael)
    RVM.addReview(rMichael2)
    RVM.addReview(rGrant)
    RVM.addReview(rGrant2)
    RVM.addReview(rKenadi)
}