package com.example.ratemydoctor.docview

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.material.Button
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.ratemydoctor.confirmdialog.ConfirmDialog
import com.example.ratemydoctor.confirmdialog.ConfirmViewModel
import com.example.ratemydoctor.doclist.DocListVM
import com.example.ratemydoctor.navigation.Routes
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.LandscapeView
import com.example.ratemydoctor.ui.theme.Rating
import com.example.ratemydoctor.ui.theme.SearchBar

@ExperimentalFoundationApi
@Composable
fun DocView(
    ratings: List<Rating>,
    selectedReview: Rating?,
    confirmVM: ConfirmViewModel,
    waiting: Boolean,
    waitingProgress: Float,
    onDelete: suspend (Rating) -> Unit,
    onFilter: (String) -> Unit,
    onSelectReview: (Rating) -> Unit,
    vm: DocListVM,
    nav: NavHostController
) {
    confirmVM.waiting.value = waiting
    confirmVM.waitingProgress.value = waitingProgress

    val doc = vm.selected.value
    val avgRating = calcAvg(ratings, doc)

    Box(contentAlignment = Alignment.Center) {
        ConfirmDialog(
            title = "Deleting",
            text = "Are you sure you want to delete?",
            confirmViewModel = confirmVM
        )
        Column(
            modifier = Modifier.alpha(if(waiting) 0.2f else 1.0f)
        ) {

            SearchBar(onFilter = onFilter)

            // Shows doc name and average rating at top
            Card(
                shape = RoundedCornerShape(5.dp),
                elevation = 16.dp,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp, top = 5.dp, bottom = 5.dp)
                    .fillMaxWidth()
            ) {
                Column(
                    modifier = Modifier.padding(5.dp).fillMaxHeight(0.2f).fillMaxWidth(0.5f),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Row(
                        modifier = Modifier.padding(2.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        if (doc != null) {
                            Text(doc.name, fontSize = 14.sp)
                        }
                    }

                    Row(
                        modifier = Modifier.padding(2.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text("Average Rating: $avgRating", fontSize = 14.sp)
                    }

                    Column(
                        modifier = Modifier.weight(1.0f).padding(2.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Button(
                            onClick = { nav.navigate(Routes.NewReview.route) },
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Text("New Review")
                        }
                    }
                }
            }


            val con = LocalConfiguration.current
            if (con.orientation == Configuration.ORIENTATION_PORTRAIT) {
                LazyColumn {
                    itemsIndexed(ratings) { i, rating ->
                        for (j in ratings) {
                            if (j == rating && doc != null && doc.name == rating.docName) {
                                ReviewRow(rating, {
                                    confirmVM.showConfirmDelete(onConfirm = {
                                        onDelete(rating)
                                        calcAvg(ratings, doc)
                                    })
                                }, onSelectReview)
                            }
                        }
                    }
                }
            } else {
                LandscapeView(selectedReview?.docName) {
                    LazyColumn {
                        itemsIndexed(ratings) { i, rating ->
                            for (j in ratings) {
                                if (j == rating && doc != null && doc.name == rating.docName) {
                                    ReviewRow(rating, {
                                        confirmVM.showConfirmDelete(onConfirm = {
                                            onDelete(rating)
                                            calcAvg(ratings, doc)
                                        })
                                    }, onSelectReview)
                                }
                            }
                        }
                    }
                }
            }
        }
        if (waiting) {
            CircularProgressIndicator()
        }
    }
}

fun calcAvg(
    ratings: List<Rating>,
    doc: Doc?
): Double {
    var avg = 0.0
    var count = 0
    for (rating in ratings) {
        if (doc != null) {
            if (doc.name == rating.docName) {
                avg += rating.rating
                count++
            }
        }
    }
    if (count != 0) avg /= count.toDouble()
    return avg
}