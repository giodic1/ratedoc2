package com.example.ratemydoctor.docview

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.ratemydoctor.doclist.DocListVM
import com.example.ratemydoctor.navigation.Routes
import com.example.ratemydoctor.ui.theme.Rating
import java.lang.Exception

@ExperimentalComposeUiApi
@Composable
fun NewReviewView(
    RVM: NewReview = viewModel(),
    onAddReview: (Rating) -> Unit,
    vm: DocListVM,
    nav: NavHostController
) {
    val doc = vm.selected.value
    val ctx = LocalContext.current
    val (ratingTF, commentTf) = remember { FocusRequester.createRefs() }
    val keyboardController = LocalSoftwareKeyboardController.current
    LaunchedEffect(true) {
        ratingTF.requestFocus()
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            "New Review",
            fontSize = 28.sp,
            modifier = Modifier
                .padding(16.dp)
                .focusRequester(ratingTF)
        )
        OutlinedTextField(
            value = RVM.rating.value,
            onValueChange = RVM::setRating,
            placeholder = { "Rating" },
            singleLine = true,
            label = { "Rating" },
            modifier = Modifier
                .padding(14.dp)
                .focusRequester(ratingTF),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Number
            ),
            keyboardActions = KeyboardActions(
                onNext = { commentTf.requestFocus() }
            )
        )
        OutlinedTextField(
            value = RVM.comment.value,
            onValueChange = RVM::setComment,
            placeholder = {
                Text("Comments")
            },
            label = {
                Text("Comments (Optional)")
            },
            singleLine = true,
            modifier = Modifier
                .padding(14.dp)
                .focusRequester(commentTf),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { keyboardController?.hide() }
            )
        )
        Button(
            onClick = {
                try {
                    val rating = doc?.let { RVM.validate(it) }
                    if (rating != null) {
                        onAddReview(rating)
                        nav.navigate(Routes.DocPage.route)
                    }
                } catch(e: Exception) {
                    Toast.makeText(ctx, e.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        ) {
            Text("Add Review")
        }
    }
}