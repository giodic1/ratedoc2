package com.example.ratemydoctor.docview

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating

class NewReview: ViewModel() {
    private val _rating: MutableState<String> = mutableStateOf("")
    val rating: State<String> = _rating
    private val _comment: MutableState<String> = mutableStateOf("")
    val comment: State<String> = _comment

    fun setRating(rating: String) {
        _rating.value = rating
    }

    fun setComment(comment: String) {
        _comment.value = comment
    }

    fun validate(doc: Doc): Rating {
        var rating_ = 0.0
        if (rating.value.isEmpty()) {
            throw Exception("Rating needed")
        }
        try {
            rating_ = rating.value.toDouble()
            if (rating_ > 10) {
                throw Exception("Rating must be less than 10")
            }
            else if (rating_ < 0.0) {
                throw Exception("Rating must be at least 0")
            }
        } catch (exception: Exception) {
            throw Exception("Must be a number")
        }
        return Rating(doc.name, rating_, comment.value)
    }
}