package com.example.ratemydoctor.docview

import android.app.Application
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.ratemydoctor.reviewrepository.ReviewDBRepo
import com.example.ratemydoctor.ui.theme.Rating
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DocVM(app: Application): AndroidViewModel(app) {
    private val _ratings: MutableState<List<Rating>> = mutableStateOf(listOf())
    val ratings: State<List<Rating>> = _ratings
    private val _selected: MutableState<Rating?>
    val selected: State<Rating?>
    private val _waiting: MutableState<Boolean>
    val waiting: State<Boolean>
    private val _waitingProgress: MutableState<Float>
    val waitingProgress: State<Float>

    private val _repository = ReviewDBRepo(app)

    init {
        viewModelScope.launch {
            _ratings.value = _repository.getReviews()
        }
        _selected = mutableStateOf(null)
        selected = _selected
        _waiting = mutableStateOf(false)
        waiting = _waiting
        _waitingProgress = mutableStateOf(0.0f)
        waitingProgress = _waitingProgress
    }

    fun addReview(rating: Rating) {
        viewModelScope.launch {
            _waiting.value = true
            _repository.addReview(rating)
            _ratings.value = _repository.getReviews()
            _waiting.value = false
        }
    }

    fun deleteReview(rating: Rating) {
        viewModelScope.launch {
            _waiting.value = true
            _waitingProgress.value = 0.0f
            val progJob = viewModelScope.async { increment() }
            val deleteJob = viewModelScope.async { _repository.deleteReview(rating) }
            progJob.start()
            deleteJob.await()
            progJob.cancel()
            _ratings.value = _repository.getReviews()
            _waiting.value = false
            _waitingProgress.value = 0.0f
        }
    }

    private suspend fun increment() {
        while (true) {
            delay(500)
            _waitingProgress.value += (1.0f / 10.0f)
        }
    }

    fun filter(search: String) {
        viewModelScope.launch {
            _ratings.value = _repository.getReviews().filter {
                    a -> a.docName.contains(search, true)
            }
        }
    }

    fun selectReview(rating: Rating) {
        _selected.value = rating
    }
}