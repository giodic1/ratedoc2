package com.example.ratemydoctor.repository

import android.app.Application
import androidx.room.Room
import com.example.ratemydoctor.Database
import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating

class DBRepo(app: Application): IDocsRepo {
    private val db: Database
    private var _docs: List<Doc> = listOf()

    init {
        db = Room.databaseBuilder(
            app,
            Database::class.java,
            "doc.db"
        ).fallbackToDestructiveMigration().build()
    }

    @Override
    override suspend fun getDocs(): List<Doc> {
        return db.docDao().getDocs()
    }

    @Override
    override suspend fun deleteDoc(doc: Doc) {
        db.docDao().deleteDoc(doc)
    }

    @Override
    override suspend fun addDoc(doc: Doc) {
        db.docDao().addDoc(doc)
    }
}