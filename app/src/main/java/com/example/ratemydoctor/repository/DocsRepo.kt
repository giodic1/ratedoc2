package com.example.ratemydoctor.repository

import com.example.ratemydoctor.ui.theme.Doc

class DocsRepo(): IDocsRepo {
    private var _docs = listOf<Doc>()

    @Override
    override suspend fun getDocs(): List<Doc> {
        return _docs
    }

    @Override
    override suspend fun deleteDoc(doc: Doc) {
        val i = _docs.indexOf(doc)
        _docs = _docs.subList(0, i) + _docs.subList(i + 1, _docs.size)
    }

    @Override
    override suspend fun addDoc(doc: Doc) {
        _docs = listOf(doc) + _docs
    }
}