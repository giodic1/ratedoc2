package com.example.ratemydoctor.repository

import com.example.ratemydoctor.ui.theme.Doc
import androidx.lifecycle.LiveData

interface IDocsRepo {
    suspend fun getDocs(): List<Doc>
    suspend fun deleteDoc(doc: Doc)
    suspend fun addDoc(doc: Doc)
}