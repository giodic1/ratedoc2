package com.example.ratemydoctor.repository

import com.example.ratemydoctor.ui.theme.Doc
import com.example.ratemydoctor.ui.theme.Rating

class DocsMemoryRepo : IDocsRepo {
    private var _docs = listOf<Doc>()

    init {
        val doc = Doc("Bert")
        val rating = Rating(doc.name, 10.0, "")
        _docs = _docs + listOf<Doc>(doc)
    }

    @Override
    override suspend fun getDocs(): List<Doc> {
        return _docs
    }

    @Override
    override suspend fun deleteDoc(doc: Doc) {
        val idx = _docs.indexOf(doc)
        _docs = _docs.subList(0, idx) + _docs.subList(idx+1, _docs.size)
    }

    @Override
    override suspend fun addDoc(doc: Doc) {
        _docs = listOf(doc) + _docs
    }
}

