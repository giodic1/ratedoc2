package com.example.ratemydoctor

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ExperimentalComposeApi
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.ratemydoctor.navigation.NavGraph
import com.example.ratemydoctor.navigation.Routes

@ExperimentalComposeUiApi
@ExperimentalComposeApi
@ExperimentalFoundationApi
@Composable
fun MainScreen() {
    val nav = rememberNavController()
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Rate My Doctor",
                        color = Color.Black,
                    )
                }
            )
        },
        bottomBar = { BottomBar(nav) }
    ) {
        NavGraph(nav)
    }
}

@Composable
private fun BottomBar(
    nav: NavHostController
) {
    val backStateEntry = nav.currentBackStackEntryAsState()
    val currentDestination = backStateEntry.value?.destination
    BottomNavigation(
        elevation = 16.dp
    ) {
        BottomNavigationItem(
            selected = currentDestination?.route == Routes.DocList.route,
            onClick = {
                nav.navigate(Routes.DocList.route) {
                    popUpTo(Routes.DocList.route)
                }
            },
            icon = {
                Icon(Icons.Default.Home, "")
            },
            label = {
                Text("Doc List")
            }
        )
        BottomNavigationItem(
            selected = currentDestination?.route == Routes.NewDoc.route,
            onClick = {
                nav.navigate(Routes.NewDoc.route) {
                    launchSingleTop = true
                }
            },
            icon = {
                Icon(Icons.Default.Add, "")
            },
            label = {
                Text("New Doc")
            }
        )
    }
}