package com.example.ratemydoctor.ui.theme

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Doc(
    @PrimaryKey
    val name: String
)