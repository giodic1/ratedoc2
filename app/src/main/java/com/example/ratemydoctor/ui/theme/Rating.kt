package com.example.ratemydoctor.ui.theme

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Rating(
    @PrimaryKey
    val docName: String,
    val rating: Double,
    val comments: String
)